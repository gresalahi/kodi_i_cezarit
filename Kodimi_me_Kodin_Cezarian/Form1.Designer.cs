﻿namespace Kodimi_me_Kodin_Cezarian
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PlainTextLabel = new System.Windows.Forms.Label();
            this.txtOrigjinal = new System.Windows.Forms.TextBox();
            this.ChipherTextLabel = new System.Windows.Forms.Label();
            this.txtCipher = new System.Windows.Forms.TextBox();
            this.KeyLabel = new System.Windows.Forms.Label();
            this.txtKey = new System.Windows.Forms.TextBox();
            this.btnEnkripto = new System.Windows.Forms.Button();
            this.btnDekripto = new System.Windows.Forms.Button();
            this.btnDil = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // PlainTextLabel
            // 
            this.PlainTextLabel.AutoSize = true;
            this.PlainTextLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PlainTextLabel.Location = new System.Drawing.Point(12, 27);
            this.PlainTextLabel.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.PlainTextLabel.Name = "PlainTextLabel";
            this.PlainTextLabel.Size = new System.Drawing.Size(68, 13);
            this.PlainTextLabel.TabIndex = 0;
            this.PlainTextLabel.Text = "Plain Text:";
            this.PlainTextLabel.Click += new System.EventHandler(this.label1_Click);
            // 
            // txtOrigjinal
            // 
            this.txtOrigjinal.Location = new System.Drawing.Point(88, 24);
            this.txtOrigjinal.Multiline = true;
            this.txtOrigjinal.Name = "txtOrigjinal";
            this.txtOrigjinal.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtOrigjinal.Size = new System.Drawing.Size(300, 50);
            this.txtOrigjinal.TabIndex = 1;
            // 
            // ChipherTextLabel
            // 
            this.ChipherTextLabel.AutoSize = true;
            this.ChipherTextLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChipherTextLabel.Location = new System.Drawing.Point(12, 91);
            this.ChipherTextLabel.Name = "ChipherTextLabel";
            this.ChipherTextLabel.Size = new System.Drawing.Size(72, 13);
            this.ChipherTextLabel.TabIndex = 2;
            this.ChipherTextLabel.Text = "CipherText:";
            // 
            // txtCipher
            // 
            this.txtCipher.Location = new System.Drawing.Point(88, 88);
            this.txtCipher.Multiline = true;
            this.txtCipher.Name = "txtCipher";
            this.txtCipher.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtCipher.Size = new System.Drawing.Size(300, 50);
            this.txtCipher.TabIndex = 3;
            // 
            // KeyLabel
            // 
            this.KeyLabel.AutoSize = true;
            this.KeyLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KeyLabel.Location = new System.Drawing.Point(12, 159);
            this.KeyLabel.Name = "KeyLabel";
            this.KeyLabel.Size = new System.Drawing.Size(32, 13);
            this.KeyLabel.TabIndex = 4;
            this.KeyLabel.Text = "Key:";
            // 
            // txtKey
            // 
            this.txtKey.Location = new System.Drawing.Point(88, 156);
            this.txtKey.Name = "txtKey";
            this.txtKey.Size = new System.Drawing.Size(100, 20);
            this.txtKey.TabIndex = 6;
            // 
            // btnEnkripto
            // 
            this.btnEnkripto.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnkripto.Location = new System.Drawing.Point(88, 212);
            this.btnEnkripto.Name = "btnEnkripto";
            this.btnEnkripto.Size = new System.Drawing.Size(75, 23);
            this.btnEnkripto.TabIndex = 7;
            this.btnEnkripto.Text = "Enkripto";
            this.btnEnkripto.UseVisualStyleBackColor = true;
            // 
            // btnDekripto
            // 
            this.btnDekripto.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDekripto.Location = new System.Drawing.Point(198, 212);
            this.btnDekripto.Name = "btnDekripto";
            this.btnDekripto.Size = new System.Drawing.Size(75, 23);
            this.btnDekripto.TabIndex = 8;
            this.btnDekripto.Text = "Dekripto";
            this.btnDekripto.UseVisualStyleBackColor = true;
            // 
            // btnDil
            // 
            this.btnDil.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDil.Location = new System.Drawing.Point(313, 212);
            this.btnDil.Name = "btnDil";
            this.btnDil.Size = new System.Drawing.Size(75, 23);
            this.btnDil.TabIndex = 9;
            this.btnDil.Text = "Dil";
            this.btnDil.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 262);
            this.Controls.Add(this.btnDil);
            this.Controls.Add(this.btnDekripto);
            this.Controls.Add(this.btnEnkripto);
            this.Controls.Add(this.txtKey);
            this.Controls.Add(this.KeyLabel);
            this.Controls.Add(this.txtCipher);
            this.Controls.Add(this.ChipherTextLabel);
            this.Controls.Add(this.txtOrigjinal);
            this.Controls.Add(this.PlainTextLabel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label PlainTextLabel;
        private System.Windows.Forms.TextBox txtOrigjinal;
        private System.Windows.Forms.Label ChipherTextLabel;
        private System.Windows.Forms.TextBox txtCipher;
        private System.Windows.Forms.Label KeyLabel;
        private System.Windows.Forms.TextBox txtKey;
        private System.Windows.Forms.Button btnEnkripto;
        private System.Windows.Forms.Button btnDekripto;
        private System.Windows.Forms.Button btnDil;
    }
}

